#### This project is to configure monitoring for nodejs application

##### Metrics
The project exposes metrics on /metrics endpoint. the devoloper has integrated prom-client for node js to expose the metrics.metrics to be exposed has been incoroporated 

To run the nodejs application:

    npm install 
    node app/server.js

To build the project:

    docker build -t famzyactivity/repo:nodeapp .
    docker push famzyactivity/repo:nodeapp

to deploy the app from dockerhub into my kubernetes cluster
   
   kubectl get secret docker-registry my-registry-key --docker-server=https://index.docker.io/v1/ --docker-username=famzyactivity --docker-pasword=xxxxx

   kubectl apply -f k8s-config.yaml
   kubectl port-forward svc/nodeapp 3000:3000 &
   localhost:30000

# create the dasboard
   go to grafana at localhost:8080
   click new dashboard, adda an empty panel 
   in the metrics browser paste this query rate(http_request_operations_total[2m])  save and apply
   add a new dashboard
   in the metrics browser paste this query rate(http_request_duration_seconds_sum[2m])  save and apply


