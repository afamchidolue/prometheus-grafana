## Deploy the K8S cluster in K8S Cluster
eksctl create cluster
kubectly apply -f ~/monitoring/config.yaml 

## Deploy the Prometheus operator Stack
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
kubectl create namespace monitoring
helm install monitoring prometheus-community/kube-prometheus-stack -n monitoring
helm ls

# configure Alerting for my application 
kubectl get all -n monitoring
kubectl port-forward service/monitoring-grafana 8080:80 -n monitoring &
kubectl port-forward service/monitoring-kube-prometheus-alertmanager 9093:9093 -n monitoring &
kubectl port-forward service/monitoring-kube-prometheus-prometheus  9090:9090 -n monitoring &
kubectl apply -f alert-rules.yaml
  HostHighCpuLoad Alert:
-	Expression (expr): This alert is triggered if the CPU load on a host exceeds 50%. It calculates the CPU load using the node_cpu_seconds_total metric and checks if it's less than 50% idle.
-	Time duration (for): The alert should persist for at least 2 minutes before firing.
-	severity: The severity of this alert is set to "warning."

 KubernetesPodCrashLooping Alert:
   - Expression (expr): This alert is triggered if a Kubernetes pod restarts more than 5 times (kube_pod_container_status_restarts_total` metric).
   - Time duration (for): The alert fires instantly when the condition is met (0 minutes).

#configure Third-party application (redis exporter)
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add stable https://charts.helm.sh/stable
helm repo update
helm install redis-exporter prometheus-community/prometheus-redis-exporter -f redis-values.yaml
kubectl get svc | grep redis = redis-cart port 6379
  
# project summary description
Install prometheus stack in AWS k8s
Configure Alerting for our microservice Application
Configure Alerting for a redis third party application